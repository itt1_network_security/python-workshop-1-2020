# sha256
from hashlib import sha256

pw = input('Enter Password: ')
h = sha256(pw.encode('utf-8')).digest()

N = 10
for i in range(N):
    if i != N-1:
        h = sha256(h).digest()
    else:
        h = sha256(h).hexdigest()

print(h)