# breakbcrypt.py
# Password is fernando
# 5049900000000, 1047.609375 secs - 1000 passwords (Execution of bcrypt decryption)
# 1048 secs / 1000 (passwords) = 1,048 secs (1 password)
# 1,048 secs * 1.000.000 (passwords) = 1.048.000 secs = 291.11 hours
# 1,048 secs * 10.000.000 (passwords) = 10.480.000.000 secs = 2 911 111.11 hours = 12 days

import time
import bcrypt

start = time.process_time()

f = open("rockyou-1000.txt")
lines = f.readlines()

for line in lines:
    line = line.rstrip("\n\r")
    h = "$2b$14$eAmP5DGJsS1ekQ2C5CfgxOEM9JVvWAm991MInc/CMeMh7M470Kdri"
    # pw = 'Secret1234' # changed to line.encode...
    if bcrypt.checkpw(line.encode('utf-8'),h.encode('utf-8')):
      print(line + " ---> " + "ok")
    '''  
    # Commented out not needed to see output nix every time
    else: 
      print("nix")
    '''

sum = 0
for i in range(100000):
  for j in range(1000):
    sum = sum + i + j

print("\n")
print(sum)
end = time.process_time()
print(end - start,'seconds')
