# timetest.py
# My laptop speed: 5049900000000, 13.96875 seconds
# Opgave 5 = 14.328125 seconds (1000 passwords) - Execution time for the program
# 14 secs / 1000 (passwords) = 0,014 secs (1 password)
# 0,014 secs * 1.000.000 (passwords) = 14.000 secs = 3.89 hrs
# 0,014 secs * 10.000.000 (passwords) = 140.000 secs = 38.89 hrs

import time
import hashlib

start = time.process_time()

sum = 0
for i in range(100000):
  for j in range(1000):
    sum = sum + i + j

f = open("rockyou-1000.txt")
lines = f.readlines()

# Finds the hash values of all the passwords
word_count = 0
for line in lines:
    line = line.rstrip("\n\r")
    hs = hashlib.sha256(line.encode('utf-8')).hexdigest()
    word_count += len(line.split())
    print(line + " <---> " + hs)

print("\n")
print("Count the total no. of strings: " + str(word_count))
print(sum)
end = time.process_time()
print(end - start,'seconds')
