# shatest.py
# https://www.kite.com/python/answers/how-to-generate-a-string-of-random-letters-in-python
# https://www.w3resource.com/python-exercises/string/python-data-type-string-exercise-5.php
import hashlib
import random
import string
import time

# Function to convert string "Secret1234" to hex
pw = "Secret1234"
h = hashlib.sha256()
h.update(pw.encode('utf-8'))
hex_pw = h.hexdigest()

# Generator a random string
length_of_string = random.choice(range(10, 11))
random_string = "".join(random.choice(string.ascii_letters) for i in range(length_of_string))

# Function to convert a random string to hex
h = hashlib.sha256()
h.update(random_string.encode('utf-8'))
hex_random = h.hexdigest()
hex_2digit = hex_pw[:2] + hex_random[2:]  # Replace the first 2 digits with 81

i = 1
while i <= 1:
    if hex_pw >= hex_2digit:
        print(pw + " (string) = " + hex_pw + " (hex)")
        print(random_string + " (string) = " + hex_2digit + " (hex)" + "\n")
        i += 1
        time.sleep(0.10)
    elif hex_pw == hex_2digit:
        print("No match found, please try again.")
        break
    else:
        print("Something went wrong.")
        break

