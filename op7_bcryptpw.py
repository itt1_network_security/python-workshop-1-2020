# bcryptpw.py
# The salt is the same: "$2b$14$" the rest changes after the dollar($) sign

import bcrypt

pw = "Secret1234"
hashed = bcrypt.hashpw(pw.encode('utf-8'),bcrypt.gensalt(14))
print(hashed.decode('utf-8'))
