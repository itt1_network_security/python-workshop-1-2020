# readfile.py
import hashlib

f = open("rockyou-1000.txt")
lines = f.readlines()

# Finds the hash values of all the passwords
for line in lines:
    line = line.rstrip("\n\r")
    hs = hashlib.sha256(line.encode('utf-8')).hexdigest()
    print(line + " <---> " + hs)

# Finds the password for the given hash value "cff..."
for line in lines:
    line = line.rstrip("\n\r")
    hs = hashlib.sha256(line.encode('utf-8')).hexdigest()
    hs_value = "cff47fe5d92c58d654b08b2624bbb62aa5034530e3e5eff1f4a7186cba2e03fa"

    if hs_value in hs.split():
        print("\n" + "The password is " + line + " <---> " + hs)
